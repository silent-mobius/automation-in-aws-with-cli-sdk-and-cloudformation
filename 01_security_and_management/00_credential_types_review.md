# Review of credential types

| Credentials | User |
---- | ---- | ---
| Email address + password | master account (root acess)|
| username + password | aws web console |
| access key secret key | cli/sdk |

---
# credentials best practices

- protect master credentials*
  - enable mfa
  - delete access keys
- string password policy
- do not embed in code
  - uses roles for ec2
- do not share credentials
- federation for large organizations