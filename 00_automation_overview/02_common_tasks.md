# Common tasks in aws

- create infrastructure
- password reset/credential rotation
- security group rule changes
- recourse inventory
- creating new amazon machine images
- creating ebs and rds snapshots
- cross-region copy of snapshort
- cleaning up unused resources

