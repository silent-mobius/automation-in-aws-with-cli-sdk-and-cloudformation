# installing awscli and python sdk

```sh

# refresh your repo
apt update
# or in case you are on RedHat Based Distro
yum check update

hash python3 2> /dev/null || {apt install -y python3}
hash curl 2> /dev/null || {apt install -y curl;}
hash jq 2> /dev/null || {apt install -y jq;}
# again in case of Redhat based distro, substitute apt with yum/dnf

#test all options

which python3
# should return value /usr/bin/python3

which curl
# should return value /usr/bin/curl

which jq
# should return value /usr/bin/jq


# Well also will also need python3 library manager to install aws cli and other libraries
# check with which
which pip3
# should return value /usr/bin/pip3

#lets run next command to install awscli
pip3 install awscli

# after installation is complete, check if aws is accessable
aws --version

# lets install boto3
pip3 install boto3


```