# AWS CLI Overview

- excellent for bash scripts
- common tasks can be aliased
- requires python
- Linux/Mac
  - install via pip
- Windows
  - install via installer from aws site 
- included with amazon Linux(centos clone)
- auto-discover credentials
  - ENV
  - hidden folder at ~/.aws
  - EC2  roles