# tools for automation
- AWS:
  - aws command line interface
  - aws software development kit
  - aws cloudformation
  - aws elastic beanstalk
  - aws opswork

- 3rd party
  - terraform
  - ansible
  - salt
  - puppet
  - troposphere


> NOTe: we'll mostly focus on awscli sdk and cloudformation

