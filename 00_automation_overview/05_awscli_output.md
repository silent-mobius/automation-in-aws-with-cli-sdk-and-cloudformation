# Controlling AWS CLI output

- `--filter`
  - limits what api returns
  - supported by many commands
  
- `--query`
  - limit/transforms what is displayed
  - uses JMESPATH
    - query language for json
    - link: [jmespath](https://jmespath.org)
- `--output`
  - json, txt or table

- example:

```sh
aws ec2 describe-instances \
    --filter Name=image-id,Values=ami-123456
    --query 'Reservations[*].Instances[*].[InstanceId]' \
    --output text
```