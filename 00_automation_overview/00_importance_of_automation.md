# Challanges of a manual process

- slow
- unreliable
- difficult to repeat
- lack of documentation
- less secure

---
# Benefits of automation

- fast and efficient
- reliable, consistent
- repeatable
- documentation is inherent
- much more secure