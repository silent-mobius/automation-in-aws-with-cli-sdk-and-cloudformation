# A note about the labs

- Containerized sevice as end result
- we'll be using:
  - Two separate aws accounts
  - *nix based system
  - Bash or Zsh
  - Docker >=17.03
- **Do not copy/paste**
- most labs build on one pc
- My Env:
  - Debian 10.10
  - Bash 4.4
  - Atom or VScode Editors